package com.demo.ecommerce.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.demo.ecommerce.constants.AppConstants;
import com.demo.ecommerce.dto.AppResponseDto;
import com.demo.ecommerce.dto.LoginRequestDto;
import com.demo.ecommerce.entity.User;
import com.demo.ecommerce.service.LoginService;

public class LoginControllerTest {

	@InjectMocks
	LoginController loginController;

	@Mock
	LoginService loginService;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLoginValidationFailure() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setUserName("TEST");
		loginRequestDto.setPassword("123456");
		User user = new User();
		user.setUserName("TEST");
		user.setPassword("123456");
		AppResponseDto response = new AppResponseDto();
		response.setResponseCode("0");
		response.setStatus(AppConstants.LOGIN_FAILURE);
		response.setErrorMessage("please enter correct username and password");
		when(loginService.loginValidation(user.getUserName(), user.getPassword())).thenReturn(Optional.of(user));
		AppResponseDto response1 = loginController.studentLogInValidation(loginRequestDto);
		assertEquals(response.getErrorMessage(), response1.getErrorMessage());
		assertEquals(response.getStatus(), response1.getStatus());
	}

}
