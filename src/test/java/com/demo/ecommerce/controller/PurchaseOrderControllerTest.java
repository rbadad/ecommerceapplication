package com.demo.ecommerce.controller;

import static org.mockito.Matchers.anyLong;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.demo.ecommerce.service.PurchaseOrderService;

@SpringBootTest
class PurchaseOrderControllerTest {

	@InjectMocks
	private PurchaseOrderController purchaseOrderController;
	
	@Mock
	private PurchaseOrderService purchaseOrderService;
	
	@Test
	public void testgetPurchaseHistoryDetails() {
		purchaseOrderService.getPurchaseHistoryDetails(anyLong());
		purchaseOrderController.getPurchaseHistoryDetails(1L);
	}
	@Test
	public void testgetPurchaseHistoryDetailsElse() {
		purchaseOrderService.getPurchaseHistoryDetails(anyLong());
		purchaseOrderController.getPurchaseHistoryDetails(1L);
	}

}
