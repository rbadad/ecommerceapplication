package com.demo.ecommerce.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.demo.ecommerce.dto.LoginRequestDto;
import com.demo.ecommerce.entity.User;

public class LoginServiceTest {
	@InjectMocks
	LoginService loginService;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testLoginValidationSuccess() {
		LoginRequestDto loginRequestDto = new LoginRequestDto();
		loginRequestDto.setUserName("ADMIN");
		loginRequestDto.setPassword("123456");
		Optional<User> user = loginService.loginValidation(loginRequestDto.getUserName(),
				loginRequestDto.getPassword());
		assertEquals(loginRequestDto.getUserName(), user.get().getUserName());
		assertEquals(loginRequestDto.getPassword(), user.get().getPassword());
	}

}
