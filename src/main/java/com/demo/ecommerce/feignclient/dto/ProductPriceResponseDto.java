package com.demo.ecommerce.feignclient.dto;

public class ProductPriceResponseDto {

	private Double productPrice;

	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}
}
