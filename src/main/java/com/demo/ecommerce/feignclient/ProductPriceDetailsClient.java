package com.demo.ecommerce.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.demo.ecommerce.feignclient.dto.ProductPriceResponseDto;

//@FeignClient(value="order-service", url="http://localhost:8081/demo/orders")
//@FeignClient(name = "http://ORDER-SERVICE/demo/orders")
@FeignClient(value="Product-Service", url="http://localhost:2020/products")
public interface ProductPriceDetailsClient {
	@GetMapping("/getProductPrice/{productCode}")
	public ResponseEntity<ProductPriceResponseDto> getProductPriceByProductCode(@PathVariable("productCode") String productCode);

}
