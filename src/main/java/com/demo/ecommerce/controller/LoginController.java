package com.demo.ecommerce.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecommerce.constants.AppConstants;
import com.demo.ecommerce.dto.AppResponseDto;
import com.demo.ecommerce.dto.LoginRequestDto;
import com.demo.ecommerce.entity.User;
import com.demo.ecommerce.feignclient.ProductPriceDetailsClient;
import com.demo.ecommerce.feignclient.dto.ProductPriceResponseDto;
import com.demo.ecommerce.service.LoginService;

import io.swagger.annotations.ApiOperation;

@RestController
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	@Autowired
	ProductPriceDetailsClient poductPriceDetailsClient;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@PostMapping("/login")
	@ApiOperation("ECommerce Application login")
	public AppResponseDto studentLogInValidation(@RequestBody LoginRequestDto request) {
		LOGGER.info("ECommerce Application login cred :" + request.getUserName() + "," + request.getPassword());
		AppResponseDto response = new AppResponseDto();
		LOGGER.info("!!Before calling service class!!");
		Optional<User> user = loginService.loginValidation(request.getUserName(), request.getPassword());
		if (user.isPresent())
			if (user.get().getUserName().equalsIgnoreCase("ADMIN")
					& user.get().getPassword().equalsIgnoreCase("123456")) {
				response.setResponseCode("1");
				response.setStatus(AppConstants.LOGIN_SUCCESS);
				response.setErrorMessage("");
			} else {
				LOGGER.warn("please enter the correct username and password");
				response.setResponseCode("0");
				response.setStatus(AppConstants.LOGIN_FAILURE);
				response.setErrorMessage("please enter correct username and password");
			}
		LOGGER.info("**After login service call**");
		return response;
	}
	
	@GetMapping("/getProductPrice/{productCode}")
	public ResponseEntity<ProductPriceResponseDto> getProductPriceByProductCode(@PathVariable("productCode") String productCode)
			throws Exception {
		return (ResponseEntity<ProductPriceResponseDto>) poductPriceDetailsClient.getProductPriceByProductCode(productCode);
		
	}

}
