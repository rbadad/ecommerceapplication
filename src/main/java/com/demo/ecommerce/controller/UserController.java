package com.demo.ecommerce.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecommerce.dto.OrderReqDto;
import com.demo.ecommerce.dto.OrderResDto;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.exception.ProductNotFoundException;
import com.demo.ecommerce.exception.UserNotFoundException;
import com.demo.ecommerce.feignclient.ProductPriceDetailsClient;
import com.demo.ecommerce.feignclient.dto.ProductPriceResponseDto;
import com.demo.ecommerce.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/users")
@Api(description = "Operations pertaining to user service")
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserService userService;
	
	/**
	 * search for products
	 * @param productName
	 * @param productAvailablity
	 * @return list of products
	 * @throws Exception 
	 */
	@GetMapping("/search")
	@ApiOperation("Search for products")
	public ResponseEntity<List<Product>> search(@RequestParam String productName, @RequestParam String productAvailablity) throws Exception {
		LOGGER.debug("ProductController :: search :: start");
		List<Product> productList = userService.search(productName, productAvailablity);
		LOGGER.debug("ProductController :: search :: end");
		return new ResponseEntity<>(productList,HttpStatus.OK);
	}
	
	
	/**
	 * Place the order
	 * @param orderReqDto
	 * @return OrderResDto
	 * @throws UserNotFoundException
	 */
	@ApiOperation("Place the order")
	@PostMapping("/placeOrder")
	public ResponseEntity<OrderResDto> placeOrder(@RequestBody OrderReqDto orderReqDto) throws UserNotFoundException {
		LOGGER.debug("OrderController :: placeOrder :: start");
		OrderResDto orderResDto = userService.placeOrder(orderReqDto);
		LOGGER.debug("OrderController :: placeOrder :: end");
		return new ResponseEntity<>(orderResDto,HttpStatus.CREATED);
	}
	
}
