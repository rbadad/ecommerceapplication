package com.demo.ecommerce.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecommerce.entity.Order;
import com.demo.ecommerce.exception.NoPurchaseHistoryFound;
import com.demo.ecommerce.service.PurchaseOrderService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/orders")
public class PurchaseOrderController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderController.class);

	@Autowired
	private PurchaseOrderService orderService;

	@ApiOperation("Getting purchase order details based on userId")
	@GetMapping("/{userId}")
	public ResponseEntity<List<Order>> getPurchaseHistoryDetails(@PathVariable("userId") Long userId) throws NoPurchaseHistoryFound {
		LOGGER.info("Inside purchase History method" + userId);
		List<Order> orderDetails = new ArrayList<Order>();
		if(userId!= null) {
			orderDetails = orderService.getPurchaseHistoryDetails(userId);
			if(orderDetails.size()>0) {
				return new ResponseEntity<>(orderDetails, HttpStatus.OK);
			}else {
				return new ResponseEntity<List<Order>>(HttpStatus.NOT_FOUND);	
			}
		}else {
			return new ResponseEntity<List<Order>>(HttpStatus.NOT_FOUND);	
		}
	}
}
