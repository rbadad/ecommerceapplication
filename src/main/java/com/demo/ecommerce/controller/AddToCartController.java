package com.demo.ecommerce.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.ecommerce.dto.AddToCartRequestDto;
import com.demo.ecommerce.entity.Cart;
import com.demo.ecommerce.service.AddToCartService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/carts")
public class AddToCartController {


	@Autowired
	private AddToCartService addToCartService;

	//@Autowired 
	//private CartProductService cartProductService;

	@ApiOperation("Adding products to cart")

	@PostMapping 
	public void addProductToCart(@RequestBody AddToCartRequestDto  requestDto) { 
		addToCartService.addToCart(requestDto); 
	}

	/*
	 * @GetMapping("/{userId}") public Optional<Cart>
	 * getCartDetailsOfUser(@PathVariable("userId") Long userId) { return
	 * addToCartService.getUserCartDetails(userId);
	 * 
	 * }
	 */

}
