package com.demo.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.ecommerce.entity.CartProduct;

public interface CartProductRepository extends JpaRepository<CartProduct, Long>{

}
