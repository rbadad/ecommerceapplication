package com.demo.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.ecommerce.entity.Order;
import com.demo.ecommerce.exception.NoPurchaseHistoryFound;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<Order, Long>{

	public List<Order> findByUserId(Long userId) throws NoPurchaseHistoryFound;

}
