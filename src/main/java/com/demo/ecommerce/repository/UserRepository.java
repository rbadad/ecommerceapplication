package com.demo.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.ecommerce.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
