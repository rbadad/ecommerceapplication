package com.demo.ecommerce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.ecommerce.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("select p from Product p where p.productAvailablity =:productAvailablity and p.productName like %:productName%")
	public List<Product> findByProductNameAndProductAvailability(@Param("productName") String productName,
			@Param("productAvailablity") String productAvailablity);

}
