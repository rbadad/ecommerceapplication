package com.demo.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.ecommerce.entity.Cart;

/**
 * 
 * @author janbee
 *
 */
@Repository
public interface CartRepository extends JpaRepository<Cart, Long>{

	public Cart findByUserId(Long userId);
	
}
