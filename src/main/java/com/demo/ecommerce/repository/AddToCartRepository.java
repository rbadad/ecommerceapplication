package com.demo.ecommerce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.ecommerce.entity.Cart;

@Repository
public interface AddToCartRepository extends JpaRepository<Cart, Long>{
	
}
