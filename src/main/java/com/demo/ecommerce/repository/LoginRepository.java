package com.demo.ecommerce.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.demo.ecommerce.entity.User;

@Repository
public interface LoginRepository extends JpaRepository<User, Long> {

	@Query(value = "select * FROM USER s WHERE s.userName = :userName AND s.password = :password", nativeQuery = true)
	public Optional<User> findStudentByUserNameAndPasswordParams(@Param("userName") String userName,
			@Param("password") String password);

}