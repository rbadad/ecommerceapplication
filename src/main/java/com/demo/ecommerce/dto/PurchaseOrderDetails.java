package com.demo.ecommerce.dto;

import java.time.LocalDate;

public class PurchaseOrderDetails {

	private Long orderId;
	private String status;
	private LocalDate orderDate;
	private String productName;
	private double productPrice;
	private double totalProduct;
		
	public PurchaseOrderDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public LocalDate getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	public double getTotalProduct() {
		return totalProduct;
	}
	public void setTotalProduct(double totalProduct) {
		this.totalProduct = totalProduct;
	}
	@Override
	public String toString() {
		return "PurchaseOrderResponse [orderId=" + orderId + ", status=" + status + ", orderDate=" + orderDate
				+ ", productName=" + productName + ", productPrice=" + productPrice + ", totalProduct=" + totalProduct
				+ "]";
	}
}
