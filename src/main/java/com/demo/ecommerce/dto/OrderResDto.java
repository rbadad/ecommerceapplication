package com.demo.ecommerce.dto;

public class OrderResDto {

	private Long ordereId;
	private String status;
	public Long getOrdereId() {
		return ordereId;
	}
	public void setOrdereId(Long ordereId) {
		this.ordereId = ordereId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public OrderResDto() {
		super();
	}
	public OrderResDto(Long ordereId, String status) {
		super();
		this.ordereId = ordereId;
		this.status = status;
	}
	
	
}
