package com.demo.ecommerce.dto;

/**
 * 
 * @author janbee
 *
 */
public class OrderReqDto {

	private String status;
	private Long userId;
	
	public OrderReqDto() {
		super();
	}
	public OrderReqDto(String status, Long userId) {
		super();
		this.status = status;
		this.userId = userId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	
}
