package com.demo.ecommerce.dto;

public class ProductPriceDetails {
	
	private Double productPrice;

	public ProductPriceDetails() {
		super();
	}
	public Double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Double productPrice) {
		this.productPrice = productPrice;
	}
}
