package com.demo.ecommerce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.ecommerce.entity.Order;
import com.demo.ecommerce.exception.NoPurchaseHistoryFound;
import com.demo.ecommerce.repository.PurchaseOrderRepository;

@Service
public class PurchaseOrderService {

	@Autowired
	private PurchaseOrderRepository orderRepo;
	
	
	public List<Order> getPurchaseHistoryDetails(Long userId) throws NoPurchaseHistoryFound{
		return orderRepo.findByUserId(userId);
	}
}
