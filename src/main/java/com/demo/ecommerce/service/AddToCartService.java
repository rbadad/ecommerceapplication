package com.demo.ecommerce.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.demo.ecommerce.dto.AddToCartRequestDto;
import com.demo.ecommerce.dto.ProductPriceDetails;
import com.demo.ecommerce.entity.Cart;
import com.demo.ecommerce.entity.CartProduct;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.repository.AddToCartRepository;
import com.demo.ecommerce.repository.CartProductRepository;
import com.demo.ecommerce.repository.ProductRepository;

@Service
public class AddToCartService {

	@Autowired
	private AddToCartRepository addToCartRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CartProductRepository cartProductRepository;
	
		
	public void addToCart(AddToCartRequestDto requestDto) {
		Cart cart = new Cart();
		cart.setUserId(requestDto.getUserId());
		Cart newCart = addToCartRepository.save(cart);
		CartProduct cp = new CartProduct();
		cp.setCartId(newCart.getCartId());
		cp.setProductId(requestDto.getUserId());
		cartProductRepository.save(cp);
		
		Optional<Product> product = productRepository.findById(requestDto.getProductId());
		String productCode = product.get().getProductCode();
		RestTemplate restTemplate = new RestTemplate();
		ProductPriceDetails response = restTemplate.getForObject("http://localhost:2020/products/getProductPrice/"+productCode, ProductPriceDetails.class);
		System.out.println("response" + response);
		Double productPrice = response.getProductPrice();		
		System.out.println("productPrice = "+ productPrice);
	}

	public Optional<Cart> getUserCartDetails(Long userId) {
		Optional<Cart> cart = addToCartRepository.findById(userId);
		Long cartId = cart.get().getCartId();
		
		
		return null;
	}

}
