package com.demo.ecommerce.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.demo.ecommerce.dto.AddToCartRequestDto;
import com.demo.ecommerce.dto.ProductPriceDetails;
import com.demo.ecommerce.entity.Cart;
import com.demo.ecommerce.entity.CartProduct;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.repository.AddToCartRepository;
import com.demo.ecommerce.repository.CartProductRepository;
import com.demo.ecommerce.repository.ProductRepository;

public class CartProductService {

	
	@Autowired
	private AddToCartRepository addToCartRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CartProductRepository cartProductRepository;
	
		
	public void addToCart(AddToCartRequestDto requestDto) {
		Cart cart = new Cart();
		CartProduct cp = new CartProduct();
		Product p = new Product();
		cart.setUserId(requestDto.getUserId());

		Optional<Product> product = productRepository.findById(requestDto.getProductId());
		String productCode = product.get().getProductCode();
		
		RestTemplate restTemplate = new RestTemplate();
		ProductPriceDetails response = restTemplate.getForObject("http://localhost:2020/products/getProductPrice/"+productCode, ProductPriceDetails.class);
		System.out.println("response" + response);
		Double productPrice = response.getProductPrice();		
		System.out.println("productPrice = "+ productPrice);
		//addToCartRepository.save(cart);
		cp.setCartId(cart.getCartId());
		cartProductRepository.save(cp);
	}


	public Optional<Cart> getUserCartDetails(Long userId) {
		Optional<Cart> cart = addToCartRepository.findById(userId);
		Long cartId = cart.get().getCartId();
		
		
		return null;
	}
}
