package com.demo.ecommerce.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.ecommerce.constants.AppConstants;
import com.demo.ecommerce.controller.UserController;
import com.demo.ecommerce.dto.OrderReqDto;
import com.demo.ecommerce.dto.OrderResDto;
import com.demo.ecommerce.entity.Cart;
import com.demo.ecommerce.entity.Order;
import com.demo.ecommerce.entity.Product;
import com.demo.ecommerce.entity.User;
import com.demo.ecommerce.exception.ProductNotFoundException;
import com.demo.ecommerce.exception.UserNotFoundException;
import com.demo.ecommerce.feignclient.ProductPriceDetailsClient;
import com.demo.ecommerce.feignclient.dto.ProductPriceResponseDto;
import com.demo.ecommerce.repository.CartRepository;
import com.demo.ecommerce.repository.OrderRepository;
import com.demo.ecommerce.repository.ProductRepository;
import com.demo.ecommerce.repository.UserRepository;

/**
 * 
 * @author janbee
 *
 */
@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	@Autowired
	OrderRepository orderRepository;
	@Autowired
	CartRepository cartRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	ProductRepository productRepository;

	@Autowired
	ProductPriceDetailsClient productPriceDetailsClient;

	public List<Product> search(String productName, String productAvailablity) throws Exception {
		LOGGER.info("UserService :: search :: start");
		List<Product> productList = productRepository.findByProductNameAndProductAvailability(productName,productAvailablity);
		if(productList.isEmpty()) {
			throw new ProductNotFoundException();
		} 
		for(Product product : productList) {
			String productcode = product.getProductCode();
			LOGGER.info("product code :"+productcode);
			ResponseEntity<ProductPriceResponseDto> response = productPriceDetailsClient.getProductPriceByProductCode(productcode); 
			double price = response.getBody().getProductPrice(); 
			product.setPrice(price); 
		}
		LOGGER.info("UserService :: search :: end");
		return productList;
	}


	/**
	 * Placing the order
	 * 
	 * @param orderReqDto
	 * @return OrderResDto
	 * @throws UserNotFoundException 
	 */
	public OrderResDto placeOrder(OrderReqDto orderReqDto) throws UserNotFoundException {
		LOGGER.info("OrderService :: placeOrder :: start");
		OrderResDto orderResDto = new OrderResDto();
		Order order = new Order();
		Long orderId = null;
		Long userId = orderReqDto.getUserId();
		Optional<User> user = userRepository.findById(userId);
		if(!user.isPresent()) {
			throw new UserNotFoundException();
		}
		Cart cart = cartRepository.findByUserId(userId);
		order.setCart(cart);
		order.setOrderedDate(LocalDate.now());
		order.setStatus(orderReqDto.getStatus());
		order.setUserId(userId);
		Order newOrder = orderRepository.save(order);
		if(newOrder.getStatus().equals(AppConstants.ORDERED)) {
			orderId = newOrder.getOrderId();
			orderResDto.setOrdereId(orderId);
			orderResDto.setStatus(AppConstants.ORDERED_SUCCESSFULLY);
			return orderResDto;
		} else {
			orderId = newOrder.getOrderId();
			orderResDto.setOrdereId(orderId);
			orderResDto.setStatus(AppConstants.ORDER_CANCELLED);
			return orderResDto;
		}
	}
}
