package com.demo.ecommerce.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CartProduct implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long cpId;
	private Long cartId;
	private Long productId;
	
	public CartProduct() {
		super();
		
	}

	public CartProduct(Long cpId, Long cartId, Long productId) {
		super();
		this.cpId = cpId;
		this.cartId = cartId;
		this.productId = productId;
	}

	public Long getCpId() {
		return cpId;
	}

	public void setCpId(Long cpId) {
		this.cpId = cpId;
	}

	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "CartProduct [cpId=" + cpId + ", cartId=" + cartId + ", productId=" + productId + "]";
	}
}
