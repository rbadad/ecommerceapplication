package com.demo.ecommerce.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long productId;
	private String productName;
	private String productAvailablity;
	private String productCode;
		
	@Transient
	private Double price;
	public Product() {
		super();
	}

	public Product(Long productId, String productName, String productAvailablity, String productCode) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productAvailablity = productAvailablity;
		this.productCode = productCode;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductAvailablity() {
		return productAvailablity;
	}

	public void setProductAvailablity(String productAvailablity) {
		this.productAvailablity = productAvailablity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productAvailablity="
				+ productAvailablity + ", productCode=" + productCode + "]";
	}
}
